from django.shortcuts import render, redirect, get_object_or_404
from .models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list
    }
    return render(request, "todo.html", context)


def show_todo_list(request, id):
    todo_list = TodoList.objects.get(id=id)
    context = {
        "todo_detail": todo_list
    }
    return render(request, "detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo = form.save(False)
            todo.save()
            return redirect("todo_list_detail", id=todo.id)

    else:
        form = TodoForm()
    
    context = {
        "form": form,
    }
    return render(request, "create.html", context)


def edit_list(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)

    else:
        form = TodoForm(instance=post)

    context = {
        "form": form,
        "post": post,
    }
    return render(request, "edit.html", context)


def delete_list(request, id):
    delete_object = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete_object.delete()
        return redirect("todo_list_list")

    else:
        return render(request, "delete.html")


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save(False)
            item.save()
            return redirect("todo_list_detail", id=item.list.id)

    else:
        form = TodoItemForm()
    
    context = {
        "form": form
    }
    return render(request, "create_item.html", context)


def edit_item(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            save = form.save(False)
            save.save()
            return redirect("todo_list_detail", id=save.list.id)

    else:
        form = TodoItemForm(instance=post)
    
    context = {
        "form": form,
        "post": post,
    }
    return render(request, "edit_item.html", context)